'use strict';

var argv = require('optimist').argv;

/*
argv.project --project
argv.port --port
*/

var params = {
	project : argv.project || 'default',
	port : argv.port || 3000,
	path : {
		root : '',
	},
	browserSync : {
		stream : true,
		notify : false,
	},
	babel : {
		presets : [
			//'es2015',
			'@babel/env'
		],
	},
};

params.path.root = './projects/' + params.project;
params.path.build = {
	root : params.path.root + '/build',
	js : params.path.root + '/build/js',
	css : params.path.root + '/build/css',
};
params.path.src = {
	_ : params.path.root + '/src/_',
	page : params.path.root + '/src/page',
	block : params.path.root + '/src/block',
	less : params.path.root + '/src/less',
	js : params.path.root + '/src/js',
	webpack : params.path.root + '/src/webpack',
};

var G = {
	
	gulp : require('gulp'),
	
	browserSync : require('browser-sync').create(),
	watch : require('gulp-watch'),
	concat : require('gulp-concat'),
	rename : require('gulp-rename'),
	cache : require('gulp-cache'),
	plumber : require('gulp-plumber'),
	replace : require('gulp-replace'),
	babel : require('gulp-babel'),

	less : require('gulp-less'),
	cleanCSS : require('gulp-clean-css'),
	inlineCss : require('gulp-inline-css'),
	autoprefixer : require('gulp-autoprefixer'),

	uglify : require('gulp-uglify'),

	pagebuilder2 : require('gulp-pagebuilder2'),

	webpack_stream : require('webpack-stream'),
	webpack : require('webpack'),

	htmlmin : require('gulp-htmlmin'),

}

G.browserSyncReload = G.browserSync.reload;

require(params.path.root + '/gulp-project.js')(G, params);
